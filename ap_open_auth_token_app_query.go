package alipay

import (
	"encoding/json"
)

// 查询授权信息，请求头不能包含app_auth_token公共参数
// https://opendocs.alipay.com/apis/api_9/alipay.open.auth.token.app.query
func (c Client) OpenAuthTokenAppQuery(body OpenAuthTokenAppQueryBody) (aliRsp OpenAuthTokenAppQueryResponse, err error) {
	params := BodyMap{
		"biz_content": c.GenerateBizContent(body),
	}
	bytes, err := c.doAlipay("alipay.open.auth.token.app.query", params)
	if err != nil {
		return
	}
	var response OpenAuthTokenAppQueryResponseModel
	if err = json.Unmarshal(bytes, &response); err != nil {
		return
	}
	aliRsp = response.Data
	return
}

type OpenAuthTokenAppQueryBody struct {
	AppAuthToken string `json:"app_auth_token"` // 应用授权令牌
}

type OpenAuthTokenAppQueryResponse struct {
	ResponseModel
	UserId      string   `json:"user_id"`      // 授权商户的user_id
	AuthAppId   string   `json:"auth_app_id"`  // 授权商户的appid
	ExpiresIn   int64    `json:"expires_in"`   // 应用授权令牌失效时间，单位到秒
	AuthMethods []string `json:"auth_methods"` // 当前app_auth_token的授权接口列表，["\"alipay.open.auth.token.app.query\"","\"alipay.system.oauth.token\"","\"alipay.open.auth.token.app\""]
	AuthStart   string   `json:"auth_start"`   // 授权生效时间，2015-11-03 01:59:57
	AuthEnd     string   `json:"auth_end"`     // 授权失效时间，2016-11-03 01:59:57
	Status      string   `json:"status"`       // valid：有效状态；invalid：无效状态，参见constant.go
}

type OpenAuthTokenAppQueryResponseModel struct {
	Data OpenAuthTokenAppQueryResponse `json:"alipay_open_auth_token_app_query_response"` // 返回值信息
	Sign string                        `json:"sign"`                                      // 签名，参见https://docs.open.alipay.com/291/106074
}
