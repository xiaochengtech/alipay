package alipay

import (
	"fmt"
	"testing"
)

func TestDataBillSellQuery(t *testing.T) {
	fmt.Println("----------支付宝商家账户卖出交易查询----------")
	// 初始化参数
	body := DataBillSellQueryBody{}
	body.StartTime = "2020-04-20 00:00:00"
	body.EndTime = "2020-04-21 00:00:00"
	// 请求支付
	aliRsp, err := testClient.DataBillSellQuery(body)
	if err != nil {
		t.Error(err)
		return
	}
	t.Logf("返回值: %+v\n", aliRsp)
}
