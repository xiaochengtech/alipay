package alipay

import (
	"fmt"
	"net/url"
	"strings"
)

// 第三方应用授权URL，单个授权
// https://opendocs.alipay.com/open/20160728150111277227/intro
func GetOpenAuthTokenAppUrlSingle(isProd bool, appId string, redirectUri string) (result string) {
	if isProd {
		result += "https://openauth.alipay.com"
	} else {
		result += "https://openauth.alipaydev.com"
	}
	result += "/oauth2/appToAppAuth.htm"
	result += fmt.Sprintf("?app_id=%s", appId)
	result += fmt.Sprintf("&redirect_uri=%s", url.QueryEscape(redirectUri))
	return
}

// 第三方应用授权URL，批量授权
// https://opendocs.alipay.com/open/20160728150111277227/intro
func GetOpenAuthTokenAppUrlBatch(
	isProd bool,
	appId string,
	applicationTypes []string,
	redirectUri string,
) (result string) {
	if isProd {
		result += "https://openauth.alipay.com"
	} else {
		result += "https://openauth.alipaydev.com"
	}
	result += "/oauth2/appToAppBatchAuth.htm"
	result += fmt.Sprintf("?app_id=%s", appId)
	result += fmt.Sprintf("&application_type=%s", strings.Join(applicationTypes, ","))
	result += fmt.Sprintf("&redirect_uri=%s", url.QueryEscape(redirectUri))
	return
}
