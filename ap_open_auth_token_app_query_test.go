package alipay

import (
	"fmt"
	"testing"
)

func TestOpenAuthTokenAppQuery(t *testing.T) {
	fmt.Println("----------查询应用授权令牌----------")
	// 初始化参数
	body := OpenAuthTokenAppQueryBody{
		AppAuthToken: AliAppAuthToken,
	}
	// 请求支付
	aliRsp, err := testClientNoAppAuthToken.OpenAuthTokenAppQuery(body)
	if err != nil {
		t.Error(err)
		return
	}
	fmt.Printf("返回值: %+v\n", aliRsp)
}
