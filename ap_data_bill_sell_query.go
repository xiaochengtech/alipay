package alipay

import (
	"encoding/json"
)

// 支付宝商家账户卖出交易查询
func (c Client) DataBillSellQuery(body DataBillSellQueryBody) (aliRsp DataBillSellQueryResponse, err error) {
	params := BodyMap{
		"biz_content": c.GenerateBizContent(body),
	}
	bytes, err := c.doAlipay("alipay.data.bill.sell.query", params)
	if err != nil {
		return
	}
	var response DataBillSellQueryResponseModel
	if err = json.Unmarshal(bytes, &response); err != nil {
		return
	}
	aliRsp = response.Data
	return
}

type DataBillSellQueryBody struct {
	StartTime       string `json:"start_time"`                  // 卖出交易流水创建时间的起始范围，2019-01-01 00:00:00
	EndTime         string `json:"end_time"`                    // 卖出交易流水创建时间的结束范围。与起始时间间隔不超过31天。查询结果为起始时间至结束时间的左闭右开区间
	AlipayOrderNo   string `json:"alipay_order_no,omitempty"`   // 支付宝交易流水号。如果查询参数中指定流水号，则只查询流水号相关的记录
	MerchantOrderNo string `json:"merchant_order_no,omitempty"` // 商户交易号。如果查询参数中指定交易号，则只查询相关的记录
	StoreNo         string `json:"store_no,omitempty"`          // 门店编号，模糊搜索
	PageNo          string `json:"page_no,omitempty"`           // 分页号，从1开始
	PageSize        string `json:"page_size,omitempty"`         // 分页大小1000-2000，默认2000
}

type DataBillSellQueryResponse struct {
	ResponseModel
	PageNo     string                                `json:"page_no"`     // 分页号，从1开始
	PageSize   string                                `json:"page_size"`   // 分页大小1000-2000
	TotalSize  string                                `json:"total_size"`  // 账务明细总数。返回满足查询条件的明细的数量
	DetailList []DataBillSellQueryResponseDetailItem `json:"detail_list"` // 交易流水详情
}

type DataBillSellQueryResponseDetailItem struct {
	GmtCreate       string `json:"gmt_create"`              // 交易创建时间，2019-01-01 00:00:00
	GmtPay          string `json:"gmt_pay"`                 // 交易支付时间
	GmtRefund       string `json:"gmt_refund,omitempty"`    // 交易退款时间
	AlipayOrderNo   string `json:"alipay_order_no"`         // 支付宝订单号。对账使用，不脱敏，20190101***
	MerchantOrderNo string `json:"merchant_order_no"`       // 商户订单号，创建支付宝交易时传入的信息。对账使用，不脱敏，TX***
	OtherAccount    string `json:"other_account"`           // 对方账户，张*(a*@alipay.com)
	GoodsTitle      string `json:"goods_title"`             // 商品名称
	TotalAmount     string `json:"total_amount"`            // 订单金额
	NetMdiscount    string `json:"net_mdiscount,omitempty"` // 商家优惠金额
	RefundAmount    string `json:"refund_amount,omitempty"` // 订单退款金额
	ServiceFee      string `json:"service_fee,omitempty"`   // 服务费金额
	TradeStatus     string `json:"trade_status"`            // 订单状态(待付款,成功,关闭,待发货,待确认收货,已预付,进行中)
	TradeType       string `json:"trade_type"`              // 业务类型，帮助商户作为对账参考
	StoreNo         string `json:"store_no,omitempty"`      // 门店编号
	StoreName       string `json:"store_name,omitempty"`    // 门店名称
	GoodsMemo       string `json:"goods_memo,omitempty"`    // 商品备注信息
}

type DataBillSellQueryResponseModel struct {
	Data DataBillSellQueryResponse `json:"alipay_data_bill_sell_query_response"` // 返回值信息
	Sign string                    `json:"sign"`                                 // 签名，参见https://docs.open.alipay.com/291/106074
}
